import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularApp';

  menuClick: boolean = true;
  
  clickButtonMenu(){
    this.menuClick = !this.menuClick;
  }
}
