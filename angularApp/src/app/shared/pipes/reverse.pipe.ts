import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  str = ''

  transform(value: any, ...args: any[]): any {
    console.log(value);

    for(let i=value.length-1; i>=0; i--){
      this.str += value[i]
    }
    console.log(this.str)
    return this.str;
  }

}
