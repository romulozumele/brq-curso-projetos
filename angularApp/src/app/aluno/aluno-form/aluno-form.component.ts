import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlunoService } from '../aluno.service';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit {
  meuForm: FormGroup;

  selectedCurso: String;
  curso: String;
  viewCurso: String;

  constructor(private formBuilder : FormBuilder,
          private actRoute: ActivatedRoute,
          private alunoService: AlunoService) { }

  ngOnInit() {

    this.meuForm = this.formBuilder.group(
      {
        id: ['', [ Validators.required ] ],
        nome: ['', [ Validators.required] ],
        idade: ['', [Validators.required] ],
        //curso: ['', [ Validators.required] ],
      }
    )
  }

  onSubmit(){
    let aluno = {
      id: null,
      nome: this.meuForm.value.nome,
      idade: this.meuForm.value.idade,
      curso: this.meuForm.value.curso
    }

    this.alunoService.addAluno(aluno).subscribe( 
      (response) => {
        console.log(response)
      })
  }
}

