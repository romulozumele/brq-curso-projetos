import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlunoComponent } from './aluno.component';
import { AlunoRoutingModule} from './aluno-routing.module'

import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';


@NgModule({
  declarations: [
    AlunoComponent,
    AlunoFormComponent
  ],
  
  imports: [
    CommonModule,
    FormsModule,
    AlunoRoutingModule,
    ReactiveFormsModule
  ],
  exports : [AlunoComponent]
})
export class AlunoModule { }
