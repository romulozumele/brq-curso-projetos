import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { EnService } from '../en.service';


@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  alunos;

  constructor(private http: HttpClient,
      private envService: EnService) { 
    this.alunos = this.http.get(`${this.envService.urlAPI}/alunos`)
  }

  getAll(){
    return this.alunos;
  }

  findById(idaluno){
    return this.http.get(`${this.envService.urlAPI}/alunos${idaluno}`);
  }

  addAluno(aluno){
    return this.http.post(`${this.envService.urlAPI}/alunos`, aluno);
  }
}