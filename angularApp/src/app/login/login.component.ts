import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../guards/auth/auth.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  meuForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService : AuthService) { }

  ngOnInit() {
    this.meuForm = this.formBuilder.group(
      {
        email: ['', [Validators.required] ],
        senha: ['', [Validators.required] ]
      }
    );
  }

  onSubmit(){
    console.log(this.meuForm)
    this.authService.goLogin(this.meuForm.value.email, this.meuForm.value.senha)
  }

}
