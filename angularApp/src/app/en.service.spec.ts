import { TestBed } from '@angular/core/testing';

import { EnService } from './en.service';

describe('EnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnService = TestBed.get(EnService);
    expect(service).toBeTruthy();
  });
});
