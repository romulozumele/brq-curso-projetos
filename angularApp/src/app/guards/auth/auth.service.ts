import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAutenticado: boolean = false;

  constructor(private router: Router) { }

  goLogin(email: string, senha: string){

    if(email == 'a' && senha == 'a'){
      //login aprovado
      this.isAutenticado = true;
      this.router.navigate(['professor'])
    }else{
      //redirecionar para login
      this.isAutenticado = false;
      this.router.navigate(['login'])
    }
  }

  getIsAutenticado(){
    return this.isAutenticado;
  }

  doLogout(){
    this.isAutenticado = false;
    this.router.navigate(['home'])
  }

  getIsLogout(){
    return this.isAutenticado;
  }
}
