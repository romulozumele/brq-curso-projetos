import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { LoginComponent } from '../login/login.component';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private authService : AuthService,
              private router : Router) { }

  canActivate(){

    let isAuth = this.authService.getIsAutenticado();
    let isLogout = this.authService.getIsLogout();
    
    if(isAuth == false){
      this.router.navigate(['login']);
      return isAuth;
    }

    if(isAuth == true && isLogout == true){
      this.router.navigate(['login'])
      return isLogout;
    }
   
  }
}
