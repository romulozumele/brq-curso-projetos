import { Component, OnInit } from '@angular/core';
import { ProfessorService } from './professor.service';
import { ProfessorModel } from '../shared/models/professor.model';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.css']
})
export class ProfessorComponent implements OnInit {

  nomeProfessor : any = "Romulo";
  url : string = "http://google.com.br";

  professores : ProfessorModel[] = [];
  contador : number = 0;
  isMostrarMensagem : boolean = true;
  isMouseOver : boolean = false;
  vetorCaina : string[] = [];

  constructor(private ps : ProfessorService) {
  }

  receberEvento(eventoFilho){
    console.log(eventoFilho);
  }

  getBackgroundColor(){
    this.isMouseOver == true ? 'yellow' : 'white';
  }

  onMouseOver(){
    console.log("onMouseOver")
    this.isMouseOver = true;
    this.vetorCaina.push("Cainã")
  }

  onMouseLeave(){
    console.log("onMouseLeave")
    this.isMouseOver = false;
    this.vetorCaina.push("Cainã")
  }

  addContador(){
    this.contador ++;
  }

  removeContador(){
    this.contador --;
      if(this.contador < 0){
        this.contador = 0
      }
  }
  
  devoMostrarMensagem(){
    this.isMostrarMensagem = !this.isMostrarMensagem;
  }

 /* function(data){
    console.log(data);
    this.professores = data;*/

  ngOnInit() {
   this.ps.getAll().subscribe(
    (meudado) => {
      console.log(this.professores);
      this.professores = meudado
    }
   );
  }

  deleteProf(idProfessor){

    

    this.ps.deleteProf(idProfessor).subscribe(
      (response) => {

       /* let index = -1;
         for (let i = 0 ; i < this.professores.length ; i++){
           if (this.professores[i].id == idProfessor){
             index = i;
             break;
           }
         }
         this.professores.splice( index, 1);*/
        console.log(this.professores);
        console.log(idProfessor)
         this.professores
          .splice(
            this.professores.findIndex( p => p['idprofessor'] == idProfessor)
            , 1);           
      }
    );
  }

  getNomeProfessor() : string{
    let numero : number = 10;
    return this.nomeProfessor;  
  }

  mostrarMensagem(nomeNovoProfessor){
    alert('O novo Professor é '+ nomeNovoProfessor)
  }

  mostrarMensagemBotao(valor){
    alert (valor);
  }
  onNomeChange(digitado){
    this.nomeProfessor = digitado;
  }
}
