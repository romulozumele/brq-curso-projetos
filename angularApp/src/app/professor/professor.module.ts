import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfessorRoutingModule} from './professor-routing.module';

import { ProfessorComponent } from './professor.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProfessorDetalhesComponent } from './professor-detalhes/professor-detalhes.component';
import { ProfessorFormComponent } from './professor-form/professor-form.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProfessorComponent,
    ProfessorDetalhesComponent,
    ProfessorFormComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ProfessorRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    ProfessorComponent
  ]
})
export class ProfessorModule { }
