import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfessorService } from '../professor.service';
import { ProfessorModel } from 'src/app/shared/models/professor.model';


@Component({
  selector: 'app-professor-form',
  templateUrl: './professor-form.component.html',
  styleUrls: ['./professor-form.component.css']
})
export class ProfessorFormComponent implements OnInit {
  meuForm: FormGroup
  isEdicao = false;
  idRota;

  professor = new ProfessorModel();

  constructor(private formBuilder: FormBuilder, 
              private actRoute: ActivatedRoute,
              private profService: ProfessorService, 
              private router: Router) { 

    }

  ngOnInit() {
    //Criando o meu formulário
    this.meuForm = this.formBuilder.group(
      {
        professor : this.formBuilder.group({
          id: ['', [Validators.required] ],
          nome: ['', [Validators.required] ]
        })
      }
    );

    this.actRoute.params.subscribe ( (data) => {
      this.idRota = data.id;
      console.log("ID da Rota: " + this.idRota);

      if(this.idRota){
        console.log('Edição')
        this.isEdicao = true;

        this.profService.findById(this.idRota).subscribe(
          (professorData) => {
            this.meuForm.patchValue(
              {
                professor: {       //ATRIBUTOS DA API
                  id: professorData['idprofessor'],
                  nome: professorData['nome']
                }
              }
            )
          }
        )

      }else{
        console.log('Criação')
        this.isEdicao = false;
      }
    });
  }

  isErrorCampo(nomeCampo){
    return ( !this.meuForm.get(nomeCampo).valid && this.meuForm.get(nomeCampo).touched)
  }

  getCampo(nomeCampo){
    return this.meuForm.get(nomeCampo);
  }

  onSubmit(){
    console.log(this.meuForm);
    if(this.isEdicao == false){
      /*let professor = {
        id: null,
        nome: this.meuForm.value.nome
      }*/
      this.profService.addProf(this.meuForm.value.professor).subscribe( 
        (response) => {
          console.log(response);
          this.router.navigate( ['/professores'])
      },
        (error) => {
          console.log('error');
          if(error.status == 0){
            console.log('ERROR COM STATUS 0!!')
          }
          console.log(error)
      });
    }else{
      let professor = {
        id: this.meuForm.value.idprofessor,
        nome: this.meuForm.value.nome
      }
      this.profService.updateProf(this.idRota, this.meuForm.value.professor).subscribe( 
        (response) => {
          console.log(response);
          this.router.navigate( ['/professores'])
      })
    }
  }

}
