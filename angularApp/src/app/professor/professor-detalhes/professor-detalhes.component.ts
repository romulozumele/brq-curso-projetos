import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfessorService } from '../professor.service';

@Component({
  selector: 'app-professor-detalhes',
  templateUrl: './professor-detalhes.component.html',
  styleUrls: ['./professor-detalhes.component.css']
})
export class ProfessorDetalhesComponent implements OnInit {

  idade : number = 0;

  @Input("profFilho") todosProfessores = [];

  @Output("") emitirEvento = new EventEmitter;

  addContador(){
    this.idade++;
    this.emitirEvento.emit( {"id":1, "idade":this.idade} );
  }

  removeContador(){
    this.idade--;
      if(this.idade < 0){
        this.idade = 0;
      }
      this.emitirEvento.emit( {"id":1, "idade":this.idade} );
  }

  id : number;
  professor : any;

  constructor(private ar : ActivatedRoute, private profService : ProfessorService) { 
    
    this.ar.params.subscribe( (data) => {
      console.log(data);
      this.id = data.id;
      
    } );

   this.profService.findById(this.id).subscribe( (professor)  => {
      this.professor = professor;
      console.log(this.professor);
    } );
  }

  ngOnInit() {
  }

}
