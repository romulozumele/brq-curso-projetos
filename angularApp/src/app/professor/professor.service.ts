import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment'
import { EnService } from '../en.service';
import { ProfessorModel } from '../shared/models/professor.model';

import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  professores;
 /*professores: any = [
    {
      idprofessor: 1,
      nome: "Romulo"
    }
  ]*/

  constructor(private http: HttpClient, private envService : EnService) {
    this.professores = this.http.get<ProfessorModel[]>(`${this.envService.urlAPI}/professor`);
   }

  getAll(){
    return this.professores;
  }

  findById(idprofessor){
    return this.http.get<ProfessorModel>(`${this.envService.urlAPI}/professor/${idprofessor}`)
   }

   addProf( professor ){
    return this.http.post<ProfessorModel>(`${this.envService.urlAPI}/professor`, professor);

   }

   updateProf(idprofessor, professor){
     return this.http.patch<ProfessorModel>(`${this.envService.urlAPI}/professor/${idprofessor}`, professor);
   }

   deleteProf( idprofessor){
      return this.http.delete<ProfessorModel>(`${this.envService.urlAPI}/professor/${idprofessor}`);
   }
}
