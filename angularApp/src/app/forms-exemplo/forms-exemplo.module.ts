import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { FormsExemploRoutingModule } from './forms-exemplo-routing.module';
import { TemplateDrivenComponent } from './template-driven/template-driven.component';
import { DataDrivenComponent } from './data-driven/data-driven.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { CicloComponent } from './ciclo/ciclo.component';
import { TextoComponent } from './texto/texto.component';


@NgModule({
  declarations: [TemplateDrivenComponent, DataDrivenComponent, CicloComponent, TextoComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsExemploRoutingModule,
    SharedModule
  ]
})
export class FormsExemploModule { }
