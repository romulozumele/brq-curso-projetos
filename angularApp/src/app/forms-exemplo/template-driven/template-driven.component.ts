import { Component, OnInit } from '@angular/core';
import { FormsExemploService } from '../forms-exemplo.service';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css']
})
export class TemplateDrivenComponent implements OnInit {

  constructor(private formService : FormsExemploService){

  }
  emailModel: String = "romulo.zuemele@gmail.com";
  senhaModel: String;
  
  ngOnInit() {
  }

  isErrorCampo(campo){
    return (campo.valid == false && campo.touched == true);
  }

  onSubmit(meuForm){
    console.log(meuForm)
    /*this.formService.postProfessor().subscribe( (dado ) => {
     
    })
    console.log(meuForm);*/
  }

}
