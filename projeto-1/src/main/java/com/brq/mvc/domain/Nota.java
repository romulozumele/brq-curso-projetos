package com.brq.mvc.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="nota")
public class Nota implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idnota;
	
	@Column(name="valor")
	private int valor;
	
	@Column(name="nome")
	private String nome;
	
	public Nota() {
	}
	
	public Nota(int idnota, int valor, String nome) {
		super();
		this.idnota = idnota;
		this.valor = valor;
		this.nome = nome;
	}
	
	public int getIdnota() {
		return idnota;
	}
	public void setIdnota(int idnota) {
		this.idnota = idnota;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idnota;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nota other = (Nota) obj;
		if (idnota != other.idnota)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Nota [idnota=" + idnota + ", valor=" + valor + ", nome=" + nome + "]";
	}
	
	

}
 