package com.brq.mvc.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name="professor")
public class Professor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROFESSOR_NAME_SEQ")
    @SequenceGenerator(sequenceName = "professor_seq", allocationSize = 1, name = "PROFESSOR_NAME_SEQ")
	private int idprofessor;
	
	@Column
	private String nome;
	
	public Professor() {
		
	}
	
	public Professor(int id, String nome) {
		super();
		this.idprofessor = id;
		this.nome = nome;
	}

	public int getIdprofessor() {
		return idprofessor;
	}

	public void setIdprofessor(int idprofessor) {
		this.idprofessor = idprofessor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Professor [id=" + idprofessor + ", nome=" + nome + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idprofessor;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		if (idprofessor != other.idprofessor)
			return false;
		return true;
	}
	
	

	
}
