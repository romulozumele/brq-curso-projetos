package com.brq.mvc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.brq.mvc.domain.Professor;
import com.brq.mvc.repositories.ProfessorRepository;

@Service
public class ProfessorService {
	
	@Autowired
	private ProfessorRepository repo;
	
	public List<Professor> getAllProfessores(){
		List<Professor> listaProfessor = repo.findAll();
		return listaProfessor;
	}
	
	public Professor createProfessor(Professor prof) {
		return repo.save(prof);
	}
	
	public Optional<Professor> findById(int id) {
		return repo.findById(id);
	}
	
	public Professor updateProfessor(int id, Professor professor) {
		Optional<Professor> p = repo.findById(id);
		
		if(p.isPresent() == true) {
			p.get().setNome(professor.getNome());
			return repo.save(p.get());
		}else {
			return repo.save(professor);
		}
	}
	
	public boolean deleteProf(int id) {
		repo.deleteById(id);
		return true;
	}
	
	public Page<Professor> findPage(int pagina, int qtdLinhas, String direcao, String campo) {
		
		PageRequest pageRequest = PageRequest.of(pagina, qtdLinhas, Direction.valueOf(direcao), campo);
		return repo.findAll(pageRequest);
	}
}
