package com.brq.mvc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brq.mvc.domain.Nota;
import com.brq.mvc.repositories.NotaRepository;

@Service
public class NotaService {

	@Autowired
	private NotaRepository repo;
	
	public List<Nota> getAllNotas(){
		List<Nota> listaNota = repo.findAll();
		return listaNota;
	}
	
	public Nota createNota(Nota nota) {
		return repo.save(nota);
	}
	
	public Optional<Nota> findById(int id) {
		return repo.findById(id);
	}
	
	public boolean deleteNota(int id) {
		repo.deleteById(id);
		return true;
	}
	
	public Nota updateNota(int id, Nota nota) {
		Optional<Nota> r = repo.findById(id);
		
		if(r.isPresent() == true) {
			r.get().setValor(nota.getValor());
			r.get().setNome(nota.getNome());
			return repo.save(r.get());
		}else {
			return repo.save(nota);
		}
	}
}
