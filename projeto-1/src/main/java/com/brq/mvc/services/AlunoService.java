package com.brq.mvc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brq.mvc.domain.Aluno;
import com.brq.mvc.repositories.AlunoRepository;
import com.brq.mvc.repositories.CursoRepository;
import com.brq.mvc.repositories.ProfessorRepository;

@Service
public class AlunoService {

	@Autowired
	private AlunoRepository repo;
	
	@Autowired
	private ProfessorRepository repoProfessor;
	
	@Autowired
	private CursoRepository repoCurso;
	
	public List<Aluno> getAllAlunos(){
		List<Aluno> listaAluno = repo.findAll();
		return listaAluno;
	}
	
	public Aluno createAluno(Aluno aluno) {
		return repo.save(aluno);
	}
	
	public Optional<Aluno> findById(int id) {
		return repo.findById(id);
	}
	
	public boolean deleteAluno(int id) {
		repo.deleteById(id);
		return true;
	}
	
	public Aluno updateAluno(int id, Aluno aluno) {
		Optional<Aluno> a = repo.findById(id);
		
		if(a.isPresent() == true) {
			a.get().setNome(aluno.getNome());
			a.get().setIdade(aluno.getIdade());
			a.get().setCurso(aluno.getCurso());
			return repo.save(a.get());
		}else {
			return repo.save(aluno);
		}
	}
}

