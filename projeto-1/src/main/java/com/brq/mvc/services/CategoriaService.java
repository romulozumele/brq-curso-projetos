package com.brq.mvc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brq.mvc.domain.Aluno;
import com.brq.mvc.domain.Categoria;
import com.brq.mvc.repositories.CategoriaRepository;

@Service
public class CategoriaService {
	
	@Autowired
	CategoriaRepository repo; 
	
	public List<Categoria> getAllCategorias(){
		List<Categoria> listaCategoria = repo.findAll();
		return listaCategoria;
	}
	
	public Categoria createCategoria(Categoria categoria) {
		return repo.save(categoria);
	}
	
	public Optional<Categoria> findById(int id) {
		return repo.findById(id);
	}
	
	public List<Categoria> findByNomeContains(String nomeBusca){
		return repo.findByNomeContains(nomeBusca);
	}
	
	public boolean deleteAluno(int id) {
		repo.deleteById(id);
		return true;
	}
	
	public Categoria updateCategoria(int id, Categoria categoria) {
		Optional<Categoria> c = repo.findById(id);
		
		if(c.isPresent() == true) {
			c.get().setNome(categoria.getNome());
			
			return repo.save(c.get());
		}else {
			return repo.save(categoria);
		}
	}
}
