package com.brq.mvc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brq.mvc.domain.Produto;
import com.brq.mvc.repositories.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository repo;
	
	public List<Produto> getAllProdutos(){
		List<Produto> listaProduto = repo.findAll();
		return listaProduto;
	}
	
	public Produto createProduto(Produto produto) {
		return repo.save(produto);
	}
	
	public Optional<Produto> findById(int id){
		return repo.findById(id);
	}
	
	public boolean deleteProduto(int id) {
		repo.deleteById(id);
		return true;
	}
	
	public Produto updateProduto(int id, Produto produto) {
		Optional<Produto> p = repo.findById(id);
		
		if(p.isPresent() == true) {
			p.get().setNome(produto.getNome());
			p.get().setValor(produto.getValor());
			p.get().setCategorias(produto.getCategorias());
			return repo.save(p.get());
		}else {
			return repo.save(produto);
		}
	}
}