package com.brq.mvc.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brq.mvc.domain.Nota;
import com.brq.mvc.resources.exception.NotaNotFoundException;
import com.brq.mvc.services.NotaService;

@RestController
@RequestMapping(value="/notas")
public class NotaController {
	
	@Autowired
	private NotaService service;
	
	//List<Nota> listaProfessor = new ArrayList<Nota>();
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity< List<Nota> > getAllNotas() {
		return ResponseEntity.ok().body(service.getAllNotas());
	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<Nota> createNota(@RequestBody Nota nota) {
		return ResponseEntity.ok().body(service.createNota(nota));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity< Optional<Nota> > findById(@PathVariable("id") int id){
		Optional<Nota> obj =  service.findById(id);
			/*if(obj.isPresent() == true) {
				return ResponseEntity.ok().body(obj);
			}else {
				return ResponseEntity.badRequest().build();
			}*/
			
			//IF TERNÁRIO
			//return (obj.isPresent() == true ? ResponseEntity.ok().body(obj) :
						//ResponseEntity.badRequest().build();
		
		if (obj.isPresent() == false) {
			throw new NotaNotFoundException(id);
		}
		
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteNota(@PathVariable("id") int id) {
		return ResponseEntity.ok().body(service.deleteNota(id));
	}
	
	@RequestMapping(value="{id}", method = RequestMethod.PATCH)
	public ResponseEntity<Nota> updateNota(@RequestBody Nota nota, @PathVariable("id") int id){
		return ResponseEntity.ok().body(service.updateNota(id, nota));
	}
}
