package com.brq.mvc.resources.exception;

public class NotaNotFoundException extends RuntimeException {
	
	public NotaNotFoundException(int id) {
		super ( "A nota com o id " + id +  " não foi localizada");
	}
}
