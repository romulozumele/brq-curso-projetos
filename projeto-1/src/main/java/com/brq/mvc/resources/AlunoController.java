package com.brq.mvc.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brq.mvc.domain.Aluno;
import com.brq.mvc.domain.Nota;
import com.brq.mvc.services.AlunoService;

@RestController
@RequestMapping(value="/alunos")
public class AlunoController {

	@Autowired
	private AlunoService service;
	
	//List<Aluno> listaAluno= new ArrayList<Aluno>();
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity<List<Aluno>> getAllAlunos(){
		return ResponseEntity.ok().body(service.getAllAlunos());
	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<Aluno> createAluno(@RequestBody Aluno aluno) {
		return ResponseEntity.ok().body(service.createAluno(aluno));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity< Optional<Aluno> > findById(@PathVariable("id") int id){
		Optional<Aluno> obj =  service.findById(id);
			if(obj.isPresent() == true) {
				return ResponseEntity.ok().body(obj);
			}else {
				return ResponseEntity.badRequest().build();
			}
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteAluno(@PathVariable("id") int id) {
		return ResponseEntity.ok().body(service.deleteAluno(id));
	}
	
	@RequestMapping(value="{id}", method = RequestMethod.PATCH)
	public ResponseEntity<Aluno> updateAluno(@RequestBody Aluno aluno, @PathVariable("id") int id){
		return ResponseEntity.ok().body(service.updateAluno(id, aluno));
	}
}
