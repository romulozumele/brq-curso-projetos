package com.brq.mvc.resources;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brq.mvc.domain.Produto;
import com.brq.mvc.resources.exception.ObjectNotFoundException;
import com.brq.mvc.services.ProdutoService;

@RestController
@RequestMapping(value="/produtos")
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	@GetMapping
	public ResponseEntity<List<Produto>> getAllProdutos(){
		return ResponseEntity.ok().body(produtoService.getAllProdutos());
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity< Optional<Produto> > findById(@PathVariable("id") int id){
		Optional<Produto> obj =  produtoService.findById(id);
		
		if(obj.isPresent() == false) {
			throw new ObjectNotFoundException(id, new Produto());
		}
		
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<Produto> createProduto(@RequestBody @Valid Produto produto) {
		return ResponseEntity.ok().body(produtoService.createProduto(produto));
	}
}
