package com.brq.mvc.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brq.mvc.domain.Professor;
import com.brq.mvc.resources.exception.ObjectNotFoundException;
import com.brq.mvc.services.ProfessorService;

@RestController
@RequestMapping (value = "/professor")
public class ProfessorController {
	
	@Autowired
	private ProfessorService service;
	
	List<Professor> listaProfessor = new ArrayList<Professor>();

	@RequestMapping(method= RequestMethod.GET)
	public List<Professor> getAllProfesores() {
		return service.getAllProfessores();
	}
	
	@RequestMapping(method= RequestMethod.POST)
	public Professor criarProfessor(@RequestBody Professor prof) {
		/*Professor p1 = new Professor();
		p1.setId(1);
		p1.setNome("Romulo");
		listaProfessor.add(p1);
		
		Professor p2 = new Professor();
		p2.setId(2);
		p2.setNome("Neymar");*/
		
		return service.createProfessor(prof);
		
	}
	
	@RequestMapping(value="{id}", method = RequestMethod.PATCH)
	public ResponseEntity<Professor> updateProfessor(@RequestBody Professor professor, @PathVariable("id") int id){
		return ResponseEntity.ok().body(service.updateProfessor(id, professor));
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Optional<Professor>> findById(@PathVariable("id") int id ){
		Optional<Professor> obj = service.findById(id);
		
		if (obj.isPresent() == false) {
			throw new ObjectNotFoundException(id, new Professor());
		}
		
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteProf(@PathVariable("id") int id) {
		return ResponseEntity.ok().body(service.deleteProf(id));
	}
	
	@GetMapping(value="/page")
	public ResponseEntity< Page <Professor>> findPage(
						@RequestParam(value="pagina", defaultValue="0")int pagina,
						@RequestParam(value="qtdLinhas", defaultValue="8") int qtdLinhas,
						@RequestParam(value="direcao", defaultValue="ASC") String direcao,
						@RequestParam(value="campo", defaultValue="idprofessor") String campo) {
		
		Page<Professor> pageProfessores = service.findPage(pagina, qtdLinhas, direcao, campo);
		
		return ResponseEntity.ok().body(pageProfessores);
	}

}
