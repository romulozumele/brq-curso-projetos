package com.brq.mvc.resources;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brq.mvc.domain.Categoria;
import com.brq.mvc.domain.Nota;
import com.brq.mvc.dto.CategoriaDto;
import com.brq.mvc.resources.exception.NotaNotFoundException;
import com.brq.mvc.services.CategoriaService;

@RestController
@RequestMapping(value="/categorias")
public class CategoriaController {
	
	@Autowired
	CategoriaService service;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public ResponseEntity< List<CategoriaDto> > getAllCategorias() {
		
		List<Categoria> lista = service.getAllCategorias();
		
		List<CategoriaDto> listaDto = lista
				.stream()
				.map(obj -> new CategoriaDto( obj ))
				.collect(Collectors.toList());
		
		return ResponseEntity.ok().body( listaDto );
	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<Categoria> createCategoria(@RequestBody Categoria categoria) {
		return ResponseEntity.ok().body(service.createCategoria(categoria));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity< Optional<CategoriaDto> > findById(@PathVariable("id") int id){
		Optional<Categoria> cat =  service.findById(id);
		
		Optional <CategoriaDto> listaDto = cat
				.map(obj -> new CategoriaDto(obj));
	
		if (cat.isPresent() == false) {
			throw new NotaNotFoundException(id);
		}
		
		return ResponseEntity.ok().body(listaDto);
	}
	
	@RequestMapping(value="/nome/{nomeBusca}", method=RequestMethod.GET)
	public ResponseEntity <List< CategoriaDto >> findByName(@PathVariable("nomeBusca")String nomeBusca){
		List<Categoria> lista = service.findByNomeContains(nomeBusca);
		
		List<CategoriaDto> listaDto = lista
				.stream()
				.map(obj -> new CategoriaDto( obj ))
				.collect(Collectors.toList());
		
		return ResponseEntity.ok().body( listaDto);
	}
	
}
