package com.brq.mvc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.brq.mvc.domain.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {
	
	//SELECT * FROM categoria WHERE nome = " "
	List<Categoria> findByNome(String nome);
	
	//SELECT * FROM categoria WHERE nome like " " 
	List<Categoria> findByNomeLike(String nome);
	
	//SELECT * FROM categoria where nome like "%nome% andid = idCat
	List<Categoria> findByNomeContainsAndId(String nome, int idCat);
	
	List<Categoria> findByNomeContains(String nome);
	
	@Query("SELECT c FROM categoria c where id = :idCat")
	List<Categoria> luan(@Param("idCat") int idCat);
}