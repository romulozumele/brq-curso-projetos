package com.brq.mvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brq.mvc.domain.Curso;

public interface CursoRepository extends JpaRepository<Curso, Integer> {

}
