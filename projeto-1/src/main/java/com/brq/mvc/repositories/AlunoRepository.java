package com.brq.mvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brq.mvc.domain.Aluno;

public interface AlunoRepository extends JpaRepository<Aluno, Integer> {

} 
