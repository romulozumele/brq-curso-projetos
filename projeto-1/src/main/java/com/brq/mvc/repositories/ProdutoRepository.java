package com.brq.mvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brq.mvc.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
